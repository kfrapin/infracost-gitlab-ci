# Infracost GitLab CI

This project provides a set of GitLab CI examples for Infracost, so you can see cloud cost estimates for Terraform in merge requests 💰

<img src="screenshot.png" alt="Example screenshot" width="800px" />

Follow our [migration guide](https://www.infracost.io/docs/guides/gitlab_ci_migration/) if you used our old version of this repo.

## Quick start

The following steps assume a simple Terraform directory is being used, we recommend you use a more relevant [example](#examples) if required.

1. Retrieve your Infracost API key by running `infracost configure get api_key`. We recommend using your same API key in all environments. If you don't have one, [download Infracost](https://www.infracost.io/docs/#quick-start) and run `infracost register` to get a free API key.

2. Create a [project environment variable](https://docs.gitlab.com/ee/ci/variables/#add-a-cicd-variable-to-a-project) called `INFRACOST_API_KEY` with your API key. This should be masked. To make sure this can be used on all merge requests untick the 'Protect variable' option.

3. Add a new [Personal access token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html), or [Project access token](https://docs.gitlab.com/ee/user/project/settings/project_access_tokens.html) (for premium tier/self-managed GitLab users). This access token should have the `api` scope enabled so it is able to read and write comments on merge requests. As in step 2, create a masked project environment variable called `GITLAB_TOKEN` and to make sure this can be used on all merge requests untick the 'Protect variable' option."

4. Create required CI/CD variables for any cloud credentials that are needed for Terraform to run.

    - **Terraform Cloud/Enterprise users**: the [Terraform docs](https://www.terraform.io/cli/config/config-file#credentials-1) explain how to configure credentials for this using a config file, see [this example](examples/terraform-cloud-enterprise).
    - **AWS users**: the [Terraform docs](https://registry.terraform.io/providers/hashicorp/aws/latest/docs#environment-variables) explain the environment variables to set for this.
    - **Azure users**: the [Terraform docs](https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/guides/service_principal_client_secret) explain the options.
    - **Google users**: the [Terraform docs](https://registry.terraform.io/providers/hashicorp/google/latest/docs/guides/provider_reference#full-reference) explain the options, e.g. using `GOOGLE_CREDENTIALS`.

5. Create a new file in `.gitlab-ci.yml` in your repo with the following content.

    ```yaml
    variables:
      # If your terraform files are in a subdirectory, set TF_ROOT accordingly
      TF_ROOT: PATH/TO/TERRAFORM/CODE # Update this!

    stages:
      - plan
      - infracost

    cache:
      key: "${TF_ROOT}"
      paths:
        - ${TF_ROOT}/.terraform

    plan:
      stage: plan
      image:
        name: hashicorp/terraform:latest
        entrypoint: [""]
      before_script:
        # IMPORTANT: add any required steps here to setup cloud credentials so Terraform can run
        - cd ${TF_ROOT}
        - terraform init
      script:
        - terraform plan -out plan.cache
        - terraform show -json plan.cache > plan.json
      artifacts:
        paths:
          - ${TF_ROOT}/plan.cache
          - ${TF_ROOT}/plan.json
      rules:
        - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'

    infracost:
      stage: infracost
      image:
        # Always use the latest 0.9.x version to pick up bug fixes and new resources.
        # See https://www.infracost.io/docs/integrations/cicd/#docker-images for other options
        name: infracost/infracost:ci-0.9
        entrypoint: [""]
      dependencies:
        - plan
      script:
        # Generate Infracost JSON output, the following docs might be useful:
        # Multi-project/workspaces: https://www.infracost.io/docs/features/config_file
        # Combine Infracost JSON files: https://www.infracost.io/docs/features/cli_commands/#combined-output-formats
        # Environment variables: https://www.infracost.io/docs/integrations/environment_variables/
        - infracost breakdown --path ${TF_ROOT}/plan.json --format json --out-file infracost.json
        - infracost comment gitlab --path infracost.json --repo $CI_PROJECT_PATH --merge-request $CI_MERGE_REQUEST_IID --gitlab-server-url $CI_SERVER_URL --gitlab-token $GITLAB_TOKEN
      variables:
        INFRACOST_API_KEY: $INFRACOST_API_KEY
        GITLAB_TOKEN: $GITLAB_TOKEN # With `api` scope to post merge request comments
      rules:
        - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
    ```

6. 🎉 That's it! Send a new merge request to change something in Terraform that costs money. You should see a merge request comment that gets updated, e.g. the 📉 and 📈 emojis will update as changes are pushed!

    If there are issues, check the GitLab CI logs and [this page](https://www.infracost.io/docs/troubleshooting/).

7. Follow [the docs](https://www.infracost.io/usage-file) if you'd also like to show cost for of usage-based resources such as AWS Lambda or S3. The usage for these resources are fetched from CloudWatch/cloud APIs and used to calculate an estimate.

### Troubleshooting

#### 403 error when posting to GitLab

If you receive a 403 error when running the `infracost comment` command in your pipeline, check that the correct `$GITLAB_TOKEN` is being used; the environment variable [precedence order](https://about.gitlab.com/blog/2021/04/09/demystifying-ci-cd-variables/) docs might be helpful.

## Examples

The [examples](examples) directory demonstrates how these actions can be used in different workflows, including:
  - [Terraform directory](examples/terraform-directory): a Terraform directory containing HCL code
  - [GitLab Terraform](examples/gitlab-terraform): a Terraform directory using the gitlab-terraform Docker image
  - [Terraform plan JSON](examples/terraform-plan-json): a Terraform plan JSON file
  - [Terragrunt](examples/terragrunt): a Terragrunt project
  - [Terraform Cloud/Enterprise](examples/terraform-cloud-enterprise): a Terraform project using Terraform Cloud/Enterprise
  - [Multi-project using parallel matrix jobs](examples/multi-project/README.md): multiple Terraform projects using parallel matrix jobs
  - [Multi-Terraform workspace](examples/multi-terraform-workspace/README.md): multiple Terraform workspaces using parallel matrix jobs
  - [Private Terraform module](examples/private-terraform-module/README.md): a Terraform project using a private Terraform module
  - [Slack](examples/slack): send cost estimates to Slack

### Cost policy examples

Infracost policies enable centralized teams, who are often helping others with cloud costs, to provide advice before resources are launched, setup guardrails, and prevent human error. Follow [our docs](https://www.infracost.io/docs/features/cost_policies/) to use Infracost's native support for Open Policy Agent (OPA) policies. This enables you to see passing/failing policies in Infracost merge request comments (shown below) without having to install anything else.

![](.gitlab-ci/assets/policy-failure-gitlab.png)

If you use HashiCorp Sentinel, follow [our example](examples/sentinel) to output the policy pass/fail results into CI/CD logs.

## Comment options

For different commenting options `infracost comment` command supports the following flags:

- `--behavior <value>`: Optional, defaults to `update`. The behavior to use when posting cost estimate comments. Must be one of the following:
  - `update`: Create a single comment and update it on changes. This is the "quietest" option. Merge request followers will only be notified on the comment create (not updates), and the comment will stay at the same location in the comment history.
  - `delete-and-new`: Delete previous cost estimate comments and create a new one. Merge request followers will be notified on each comment.
  - `new`: Create a new cost estimate comment. Merge request followers will be notified on each comment.
- `--merge-request <merge-request-number>`: Required when posting a comment on a merge request. Mutually exclusive with `--commit` flag.
- `--commit <commit-sha>`: Required when posting a comment on a commit. Mutually exclusive with `--merge-request` flag.
- `--tag <tag>`:  Optional. Customize hidden markdown tag used to detect comments posted by Infracost. This is useful if you have multiple workflows that post comments to the same merge request or commit and you want to avoid them over-writing each other.

## Contributing

Issues and merge requests are welcome! For development details, see the [contributing](CONTRIBUTING.md) guide. For major changes, including interface changes, please open an issue first to discuss what you would like to change. [Join our community Slack channel](https://www.infracost.io/community-chat), we are a friendly bunch and happy to help you get started :)

If you'd like to contribute to this repo, you must [click here](https://cla-assistant.io/infracost/infracost) to sign our Contributor License Agreement manually as we have not yet setup that automation for GitLab.

## License

[Apache License 2.0](https://choosealicense.com/licenses/apache-2.0/)
