# Contributing

## Adding examples

Copy another example that is similar to what you'd like to add. If you don't know which to pick, try the `terraform-directory`.
1. Update your example's readme file with the required steps.
2. Run `npm run examples:generate_tests` to generate tests for your example.
3. Follow [updating the golden files](#updating-the-golden-files) to update the test output, review the new additions/changes to ensure they are what you expect for your example.
4. Update the [repo readme](readme.md), the [examples readme](examples/readme.md) and your new example's readme with the description of the example.
5. Send a merge request and wait for an Infracost team member to review it.

## Testing examples

Examples are tested by extracting them from the README.md files into a GitLab CI pipeline.

To extract the examples, the `npm run examples:generate_tests` script loops through the example directories, reads the READMEs and extracts any YAML code blocks between the markdown comment markers:

````
[//]: <> (BEGIN EXAMPLE: example-key)
```yml
stages:
  - plan
  - infracost

...
```
[//]: <> (END EXAMPLE)
````

The examples are then modified to run `infracost comment` command with `--dry-run` flag. Since the local runner won't be able to find a merge request or commit we use dummy values to stub merge request number/sha and repo path.

All the examples are then added to the `.gitlab-ci/examples` directory and a trigger is added to `.gitlab-ci/all-tests.yml` to trigger the example.

The script that handles extracting and modifying the examples is [./scripts/generateExamplesTest.js](./scripts/generateExamplesTest.js)

### Testing locally

You can test the examples locally with [gitlab-ci-local](https://github.com/firecow/gitlab-ci-local). To install on macOS:

```sh
brew install gitlab-ci-local
```

Install packages:

```sh
npm install
```

Setup required environment variables

```sh
cp .gitlab-ci-local-variables.yml.example .gitlab-ci-local-variables.yml
# Edit .gitlab-ci-local-variables.yml to add any required environment variables.
```

Then run the tests:

```sh
npm run examples:test
```

### Updating the golden files

You can update the golden files for the examples by running:

```sh
npm run examples:update_golden
```

If you expect golden files to be updated, but they are not, try to pull the latest docker image used by tests and then re-run them:

```sh
docker pull infracost/infracost:ci-0.9
```
