# Terraform plan JSON

This example shows how to run Infracost on GitLab CI with a Terraform plan JSON file. Using the Terraform docker image is not needed since the Infracost CLI can use the plan JSON directly.

To use it, add the following to your `.gitlab-ci.yml` file:

[//]: <> (BEGIN EXAMPLE: terraform-plan-json)
```yml
stages:
  - infracost

infracost:
  stage: infracost
  image:
    # Always use the latest 0.9.x version to pick up bug fixes and new resources.
    # See https://www.infracost.io/docs/integrations/cicd/#docker-images for other options
    name: infracost/infracost:ci-0.9
    entrypoint: [""] # Override since we're running commands below
  script:
    - infracost breakdown --path examples/terraform-plan-json/code/plan.json --format json --out-file infracost.json
    # See https://gitlab.com/infracost/infracost-gitlab-ci/#comment-options for other options.
    # Choose the commenting behavior, 'update' is a good default:
    #   update: Create a single comment and update it. The "quietest" option.
    #   delete-and-new: Delete previous comments and create a new one.
    #   new: Create a new cost estimate comment on every push.
    - |
      infracost comment gitlab --path infracost.json \
                               --repo $CI_PROJECT_PATH \
                               --merge-request $CI_MERGE_REQUEST_IID \
                               --gitlab-server-url $CI_SERVER_URL \
                               --gitlab-token $GITLAB_TOKEN \
                               --behavior update
  variables:
    INFRACOST_API_KEY: $INFRACOST_API_KEY
    GITLAB_TOKEN: $GITLAB_TOKEN # With `api` scope to post merge request comments
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
```
[//]: <> (END EXAMPLE)
