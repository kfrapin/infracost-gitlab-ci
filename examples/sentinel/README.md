# Sentinel Example

This example shows how to write [cost policies](https://www.infracost.io/docs/features/cost_policies/) with HashiCorp's [Sentinel](https://www.hashicorp.com/sentinel). For simplicity, this example evaluates policies with the Sentinel CLI (a.k.a. Sentinel Simulator). The point of this example is to show how a policy could be written against the Infracost JSON format, not how to run Sentinel, since that's tied to HashiCorp's cloud platform.

When the policy checks passes it outputs `Policy check passed.` in the logs. When the policy checks fail, the logs show the Sentinel output indicating failing policies.

Create a policy file (e.g. `policy.policy`) that checks a global parameter 'breakdown' containing the Infracost JSON: 

```policy
import "strings"

limitTotalDiff = rule {
  float(breakdown.totalMonthlyCost) < 1500
}

awsInstances = filter breakdown.projects[0].breakdown.resources as _, resource {
  strings.split(resource.name, ".")[0] is "aws_instance"
}

limitInstanceCost = rule {
  all awsInstances as _, instance {
    float(instance.hourlyCost) <= 2.00
  }
}

instanceBaseCost = func(instance) {
  cost = 0.0
 	for instance.costComponents as cc {
    cost += float(cc.hourlyCost)
  }
  return cost
}

instanceIOPSCost = func(instance) {
  cost = 0.0
 	for instance.subresources as sr {
    for sr.costComponents as cc {
      if cc.name == "Provisioned IOPS" {
        cost += float(cc.hourlyCost)
      }
    }
  }
  return cost
}

limitInstanceIOPSCost = rule {
  all awsInstances as _, instance {
    instanceIOPSCost(instance) <= instanceBaseCost(instance)
  }
}

main = rule {
  limitTotalDiff and
  limitInstanceCost and
  limitInstanceIOPSCost
}
```

Then use Sentinel to test infrastructure cost changes against the policy by adding the following to your `.gitlab-ci.yml` file:

[//]: <> (BEGIN EXAMPLE: sentinel)
```yml
stages:
  - infracost
  - policy_check

infracost:
  stage: infracost
  image:
    # Always use the latest 0.9.x version to pick up bug fixes and new resources.
    # See https://www.infracost.io/docs/integrations/cicd/#docker-images for other options
    name: infracost/infracost:ci-0.9
    entrypoint: [""] # Override since we're running commands below
  script:
    - infracost breakdown --path examples/sentinel/code/plan.json --format json --out-file examples/sentinel/infracost.json
  artifacts:
    paths:
      - examples/sentinel/infracost.json
  variables:
    INFRACOST_API_KEY: $INFRACOST_API_KEY
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'

sentinel:
  stage: policy_check
  image:
    name: hashicorp/sentinel:latest
    entrypoint: [""]
  script:
    - sentinel apply -color=false -global breakdown="$(cat examples/sentinel/infracost.json)" examples/sentinel/policy/policy.policy
    - echo "Policy check passed."
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
```
[//]: <> (END EXAMPLE)
