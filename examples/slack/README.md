# Slack example

This example shows how to send cost estimates to Slack using GitLab CI.

Slack message blocks have a 3000 char limit so the Infracost CLI automatically truncates the middle of slack-message output formats.

For simplicity, this is based off the terraform-plan-json example, which does not require Terraform to be installed.

To use it, add the following to your `.gitlab-ci.yml` file and setup a GitLab variable for `SLACK_WEBHOOK_URL`.

[//]: <> (BEGIN EXAMPLE: slack)
```yml
stages:
  - infracost

infracost:
  stage: infracost
  image:
    # Always use the latest 0.9.x version to pick up bug fixes and new resources.
    # See https://www.infracost.io/docs/integrations/cicd/#docker-images for other options
    name: infracost/infracost:ci-0.9
    entrypoint: [""] # Override since we're running commands below
  script:
    - infracost breakdown --path examples/terraform-plan-json/code/plan.json --format json --out-file infracost.json
    # See https://gitlab.com/infracost/infracost-gitlab-ci/#comment-options for other options.
    # Choose the commenting behavior, 'update' is a good default:
    #   update: Create a single comment and update it. The "quietest" option.
    #   delete-and-new: Delete previous comments and create a new one.
    #   new: Create a new cost estimate comment on every push.
    - |
      infracost comment gitlab --path infracost.json \
                               --repo $CI_PROJECT_PATH \
                               --merge-request $CI_MERGE_REQUEST_IID \
                               --gitlab-server-url $CI_SERVER_URL \
                               --gitlab-token $GITLAB_TOKEN \
                               --behavior update
    - infracost output --path infracost.json --format slack-message --out-file slack_message.json
    - |
      # Skip posting to Slack if there's no cost change
      cost_change=$(cat infracost.json | jq -r "(.diffTotalMonthlyCost // 0) | tonumber")
      if [ "$cost_change" = "0" ]; then
        echo "Not posting to Slack since cost change is zero"
        exit 0
      fi

      if [ -z "$SLACK_WEBHOOK_URL" ]; then
        echo "No \$SLACK_WEBHOOK_URL variable set. Add one to your GitLab repository"
        exit 1
      fi

      curl -X POST -H "Content-type: application/json" -d @slack_message.json $SLACK_WEBHOOK_URL
  variables:
    INFRACOST_API_KEY: $INFRACOST_API_KEY
    GITLAB_TOKEN: $GITLAB_TOKEN # With `api` scope to post merge request comments
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
```
[//]: <> (END EXAMPLE)
