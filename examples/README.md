# Examples

All examples post a single comment on merge requests, which gets updated as more changes are pushed. The examples show how to run the actions with:

- [Terraform directory](terraform-directory): a Terraform project containing HCL code
- [GitLab Terraform integration](gitlab-terraform): a Terraform project using the GitLab Terraform integration
- [Terraform plan JSON](terraform-plan-json): a Terraform plan JSON file
- [Terragrunt](terragrunt): a Terragrunt project
- [Terraform Cloud/Enterprise](terraform-cloud-enterprise): a Terraform project using Terraform Cloud/Enterprise
- [Multi-project using parallel matrix jobs](multi-project/README.md): multiple Terraform projects using parallel matrix jobs
- [Multi-Terraform workspace](multi-terraform-workspace/README.md): multiple Terraform workspaces using parallel matrix jobs
- [Slack](slack): send cost estimates to Slack

Cost policy examples:

- [OPA](https://www.infracost.io/docs/features/cost_policies/): check Infracost cost estimates against policies using Open Policy Agent
- [Sentinel](sentinel): check Infracost cost estimates against policies using Hashicorp's Sentinel 

See the [contributing](../CONTRIBUTING.md) guide if you'd like to add an example.
