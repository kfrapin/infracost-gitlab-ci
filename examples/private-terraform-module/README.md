# Terraform directory
# Private Terraform module

This example shows how to run Infracost on GitLab CI with a Terraform project that uses a private Terraform module. This requires a variable to be added to your GitLab repository called `GIT_SSH_KEY` containing a private key so that Terraform can access the private repository.

To use it, add the following to your `.gitlab-ci.yml` file:

[//]: <> (BEGIN EXAMPLE: private-terraform-module)
```yml
variables:
  # If your terraform files are in a subdirectory, set TF_ROOT accordingly
  TF_ROOT: examples/private-terraform-module/code

stages:
  - plan
  - infracost

cache:
  key: "${TF_ROOT}"
  paths:
    - ${TF_ROOT}/.terraform

plan:
  stage: plan
  image:
    name: hashicorp/terraform:latest
    entrypoint: [""]
  before_script:
    # IMPORTANT: add any required steps here to setup cloud credentials so Terraform can run
    # Add the git SSH key
    - mkdir -p .ssh
    - echo "$GIT_SSH_KEY" > .ssh/git_ssh_key
    - chmod 400 .ssh/git_ssh_key
    - export GIT_SSH_COMMAND="ssh -i $(pwd)/.ssh/git_ssh_key -o 'StrictHostKeyChecking=no'"
    - cd ${TF_ROOT}
    - terraform init
  script:
    - terraform plan -out plan.cache
    - terraform show -json plan.cache > plan.json
  artifacts:
    paths:
      - ${TF_ROOT}/plan.json
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'

infracost:
  stage: infracost
  image:
    # Always use the latest 0.9.x version to pick up bug fixes and new resources.
    # See https://www.infracost.io/docs/integrations/cicd/#docker-images for other options
    name: infracost/infracost:ci-0.9
    entrypoint: [""] # Override since we're running commands below
  dependencies:
    - plan
  script:
    - infracost breakdown --path ${TF_ROOT}/plan.json --format json --out-file infracost.json
    # See https://gitlab.com/infracost/infracost-gitlab-ci/#comment-options for other options.
    # Choose the commenting behavior, 'update' is a good default:
    #   update: Create a single comment and update it. The "quietest" option.
    #   delete-and-new: Delete previous comments and create a new one.
    #   new: Create a new cost estimate comment on every push.
    - |
      infracost comment gitlab --path infracost.json \
                               --repo $CI_PROJECT_PATH \
                               --merge-request $CI_MERGE_REQUEST_IID \
                               --gitlab-server-url $CI_SERVER_URL \
                               --gitlab-token $GITLAB_TOKEN \
                               --behavior update
  variables:
    INFRACOST_API_KEY: $INFRACOST_API_KEY
    GITLAB_TOKEN: $GITLAB_TOKEN # With `api` scope to post merge request comments
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
```
[//]: <> (END EXAMPLE)
